<?php

/**
 * Implements hook_form;
 *
 * @param type $form
 * @param type $form_state
 *
 * @return string
 */
function multistep_reg_field_mapping_form() {
  $form = array();
  $user_fields = get_user_profile_fields();

  $stored_mapping = variable_get('multistep_stored_mapping', array());
  foreach ($user_fields as $field_name => $field_label) {
    $form['user_fields_drupal_' . $field_name] = array(
      '#type' => 'item',
      '#markup' => $field_label[0] . ' (' . $field_name . ')',
    );
    $form['user_fields_drupal_value_' . $field_name] = array(
      '#type' => 'textfield',
      '#default_value' => isset($stored_mapping['user_fields_drupal_value_' . $field_name]) ? $stored_mapping['user_fields_drupal_value_' . $field_name] : '',
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('submit'),
  );
  $form['#theme'] = 'multistep_reg_form';
  return $form;
}

/**
 * Implements hook_form_validate;
 * Validating if there are two or more same mapping fields are selected
 *
 * @param type $form
 * @param type $form_state
 */
function multistep_reg_field_mapping_form_validate($form, &$form_state) {
  $submitted_values = array();
  $error = FALSE;
  foreach ($form_state['values'] as $key => $value) {
    if (preg_match('/^user_fields_drupal_value_/', $key)) {
      if ($value == '') {
        $value = -1;
      }
      if (is_numeric($value)) {
        if ($value < 1) {
          $value = -1;
        }
        $submitted_values[$key] = $value;
      }
      else {
        form_set_error($key, t('Step must be a number'));
        $error = TRUE;
      }
    }
  }
  if (!$error) {
    $steps = array_unique($submitted_values);
    $max = max($steps);
    $range = range(1, $max);
    $steps_not_present = array_diff($range, $steps);
    if (sizeof($steps_not_present) > 0) {
      $message = t('No fileds are selected for the follwoing steps') . ' ' . implode($steps_not_present, ', ');
      form_set_error('', $message);
    }
  }
  $form_state['values']['submitted_values'] = $submitted_values;
}

/**
 * Implements hook_form_submit;
 * Validating if there are two or more same mapping fields are selected
 *
 * @param type $form
 * @param type $form_state
 */
function multistep_reg_field_mapping_form_submit($form, &$form_state) {
  variable_set('multistep_stored_mapping', $form_state['values']['submitted_values']);
}

